<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Entity\User;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Curl\Curl;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_index")
     */
    public function indexAction()
    {
        $curl = new Curl();
        $res = $curl->get(
            'https://www.reddit.com/r/pics/search.json?q=kitten&limit=10&User-Agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'
        );


        $err = null;
        $json = null;

        $encoded = json_encode($res);
        $decoded = json_decode($encoded, true);

        $err = array_key_exists('error', $decoded);
        $json = array_key_exists('kind', $decoded);

        if ($err) {
            $err = $decoded['error'];
        } elseif ($json) {
            $json = $decoded['data']['children'];
            for ($i = 0; $i < 10; $i++) {
                $json[$i]['data']['date'] = null;
                $json[$i]['data']['date'] = date("Y-m-d H:i:s", $json[$i]['data']['created_utc']);

            }
        }

        return $this->render(
            'index.html.twig', [
            'error' => $err,
            'json' => $json
        ]);

    }

    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler

    )
    {
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = [
                'email' => $user->getEmail(),
                'password' => $user->getPassword()
            ];

            $user->setEmail($data['email']);
            $password = $userHandler->encodePlainPassword($data['password']);
            $user->setPassword($password);


            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('welcome');
        }
        return $this->render('sign_up.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /*    public function encodePlainPassword(string $password)
        {
            return md5($password) . md5($password . '2');
        }*/


    /**
     * @Route("/welcome", name="welcome")
     */
    public function welcomeAction()
    {
        return $this->render('welcome.html.twig');
    }

    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @return Response
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('app_index');
            }
        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/favorite/{id}", name="favorite")
     * @param $id string
     * @param ObjectManager $manager
     * @return Response
     */
    public function likeAction(string $id, ObjectManager $manager)
    {

        $like = new Favorite();

        $user = $this->getUser();

        $like->setUser($user);
        $like->setArticle($id);

        $manager->persist($like);
        $manager->flush();

        return $this->redirectToRoute('profile');
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profileAction()
    {
        $res = null;
        $favorite = new Favorite();
        $user = new User();
        $user = $this->getUser();
        $id = $user->getId();

        $favorite = $this
            ->getDoctrine()
            ->getRepository('App:Favorite')
            ->getArticleId($id);
        $user = $this->getUser();


        $url = null;
        for ($i = 0; $i < count($favorite); $i++) {


            if ($url) {
                $url .= ',' . $favorite[$i]->getArticle();

            } else {
                $url .= 'id=' . $favorite[$i]->getArticle();
            }
        }
        $curl = new Curl();
        $link = $curl->get(
            'https://www.reddit.com/api/info.json?' .$url
        );

        $err = null;
        $json = null;
        $encoded = json_encode($link);
        $decoded = json_decode($encoded, true);


        $err = array_key_exists('error', $link);
        $json = array_key_exists('kind', $link);


        if ($err) {
            $err = $decoded['error'];
        } elseif ($json) {
            $json = $decoded['data']['children'];

        }


        return $this->render('profile.html.twig', [
            'user' => $user,
            'json' => $json,
            'error' => $err
        ]);
    }

}
